#####################################################################
#                                                                   #
# unitconversions.py                                                #
#                                                                   #
# Copyright 2022, TU Vienna                                         #
#                                                                   #
# Implementation of the TimeBase AOM Driver for the labscript-suite,#
# used in the Léonard lab for Experimental Quantum Information.     #
#                                                                   #
# There are both single channel AOM driver (DIM-3000) and 5 channel #
# AOM drivers from Timebase. Each channel can be tuned on/off by a  #
# TTL signal, and the amplitude/frequency modulated with AM/FM      #
# input. This modulation is the same for both drivers, and the unit #
# conversion classes below can be used for the analog outputs for   #
# AM and FM signals.                                                #
#####################################################################


from labscript_utils.unitconversions.UnitConversionBase import UnitConversion
from labscript_utils.unitconversions.generic_frequency import FreqConversion
# from labscript_utils.unitconversions.detuning import detuning
from numpy import log10

class TimeBaseAOMDriver_AM(UnitConversion):
    # This must be defined outside of init, and must match the default hardware unit specified within the BLACS tab
    base_unit = "V"
    derived_units = ["TODO"]
    
    def __init__(self,calibration_parameters = None):            
        self.parameters = calibration_parameters
        UnitConversion.__init__(self,self.parameters)



class TimeBaseAOMDriver_FM(UnitConversion):
    # This must be defined outside of init, and must match the default hardware unit specified within the BLACS tab
    base_unit = "V"
    derived_units = ["Hz"]
    
    def __init__(self,calibration_parameters):            
        self.parameters = calibration_parameters
        self.parameters["magnitudes"] = ["M"]
        UnitConversion.__init__(self,self.parameters)

    def Hz_to_base(self,Hz):
        return (Hz-self.parameters["freq"]) / self.parameters["FM_deviation"] *10

    def Hz_from_base(self,V):
        return self.parameters["freq"] + V/10 * self.parameters["FM_deviation"]


class dBm_to_W(UnitConversion):
    base_unit = "dBm"
    derived_units = "W"

    def __init__(self, params):
        super().__init__(params)
    def W_to_base(self,W):
        return 10*log10(W/1e-3)
    def W_from_base(self,dBm):
        return 1e-3 * 10**(dBm/10)

class Hz_to_MHz(FreqConversion):
    def __init__(self, calibration_parameters = None):
        self.parameters = calibration_parameters
        if hasattr(self, 'derived_units'):
            self.derived_units += ['MHz']
        else:
            self.derived_units = ['MHz']
        UnitConversion.__init__(self,self.parameters)