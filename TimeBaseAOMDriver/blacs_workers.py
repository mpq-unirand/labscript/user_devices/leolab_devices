import time
import labscript_utils.h5_lock
import h5py
from blacs.tab_base_classes import Worker

class TimeBaseWorker(Worker):

    def init(self):
        global socket; import socket
        timeout = time.time()
        while True:
            try:
                self.client = socket.create_connection((self.hostname,self.port), timeout=2)
            except:
                # If we cannot get connection, wait and try again
                time.sleep(1)
                if time.time()-timeout > 10:
                    raise
                print("No connection, trying again.")
                continue
            break
        print(self.client.recv(200).decode())
        self.smart_cache = {f"CH-{i}":{} for i in range(1,self.num_AOM+1)}
        self.remote_values = {f"CH-{i}":{} for i in range(1,self.num_AOM+1)}
    
    def transition_to_buffered(self, device_name, h5file, initial_values, fresh):
        with h5py.File(h5file,"r") as file:
            group = file[f"devices/{device_name}"]
            for channel in group:
                if self.smart_cache[channel] == group[channel].attrs and not fresh:
                    continue
                print(f"Programming {channel}")
                self.smart_cache[channel] = dict(group[channel].attrs)
                for attr,value in group[channel].attrs.items():   
                    self.client.send(f"{channel}|{attr}:{value:.0f}\r\n".encode())
                    time.sleep(0.1)
                self.client.send(f"{channel}|Sout:1\r\n".encode())
                initial_values[channel]["gate"] = True
                initial_values[channel]["freq"] = group[channel].attrs["Sfreq"]
                initial_values[channel]["amp"] = group[channel].attrs["Sampl"] * 0.1
        return initial_values

    def abort_transition_to_buffered(self):
        return True
        
    def abort_buffered(self):
        return True
        
    def transition_to_manual(self):
        return True

    def check_remote_values(self):
        start = time.perf_counter()
        try:
            # Call revc in case the TCP server still has some other message queued to send
            self.client.recv(1000)
        except socket.timeout:
            pass # Nothing received
        for i in range(1,self.num_AOM+1):
            ch = f"CH-{i}"
            self.client.send(ch.encode()+b"|Gpar\r\n")
            time.sleep(0.2)
            try:
                Gpar = self.client.recv(1000).decode() # Rfreq:xxx|Rampl:xxx|Rout:xxx|...
                # Create parameter dictionary from message string
                par = dict(map(lambda str:str.split(":"),Gpar.split("|")[:-1]))
                self.remote_values[ch]["freq"] = int(par["Rfreq"])
                self.remote_values[ch]["amp"] = 0.1 * int(par["Rampl"])
                self.remote_values[ch]["gate"] = int(par["Rout"])
            except:
                # Sometimes the TCP servers seems to respond with a wrong answer to Gpar (e.g. b'Rpcbtemp:xxxx\r\n'),
                # maybe because of communication with the http interface or transmission errors.
                # If that happens we just use the saved values and try during the next call again.
                # If that happens during the first check, we need to restart the worker.
                self.logger.debug(f"Exception in check_remote_values of {ch}, returning values of previous check.")
                try:
                    self.client.recv(1000)
                except socket.timeout:
                    pass # Nothing was in the TCP server's buffer to send
        self.logger.debug(f"Checked remote values ({time.perf_counter()-start:.2f}s)")
        return self.remote_values

    def program_manual(self,values):
        for channel,dds in values.items():
            if dds["freq"] != self.remote_values[channel]["freq"]:
                self.client.send(f"{channel}|Sfreq:{dds['freq']}\r\n".encode())
                self.remote_values[channel]["freq"] = dds["freq"]
            if dds["amp"] != self.remote_values[channel]["amp"]:
                self.client.send(f"{channel}|Sampl:{10*dds['amp']}\r\n".encode())
                self.remote_values[channel]["amp"] = dds["amp"]
            if dds["gate"] != self.remote_values[channel]["gate"]:
                self.client.send(f"{channel}|Sout:{int(dds['gate'])}\r\n".encode())
                self.remote_values[channel]["gate"] = dds["gate"]

    def shutdown(self):
        # for i in range(1,self.num_AOM+1):
        #     ch = f"CH-{i}"
        #     self.client.send(ch.encode() + b"|Sout:0\r\n")
        self.client.shutdown(socket.SHUT_RDWR)
        self.client.close()