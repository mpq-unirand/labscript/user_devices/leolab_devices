import labscript_devices


labscript_device_name = "TimeBaseAOMDriver"
blacs_tab = "user_devices.TimeBaseAOMDriver.blacs_tabs.TimeBaseTab"
parser = None

labscript_devices.register_classes(
    labscript_device_name=labscript_device_name,
    BLACS_tab=blacs_tab,
    runviewer_parser=parser,
)