from blacs.tab_base_classes import MODE_MANUAL, MODE_TRANSITION_TO_BUFFERED, MODE_TRANSITION_TO_MANUAL, MODE_BUFFERED, define_state
from blacs.device_base_class import DeviceTab

class TimeBaseTab(DeviceTab):
    def initialise_GUI(self):  
        connection = self.settings['connection_table'].find_by_name(self.device_name)
        props = connection.properties
        self.hostname = props["hostname"]
        self.port = props["port"]
        self.num_AOM = props["num_AOM"]
        # Capabilities
        self.base_units =    {'freq':'Hz',                 'amp':'dBm',}
        self.base_min =      {'freq':props["min_freq"],    'amp':props["min_amp"],}
        self.base_max =      {'freq':props["max_freq"],    'amp':props["max_amp"],}
        self.base_step =     {'freq':1e6,                  'amp':0.1,}
        self.base_decimals = {'freq':0,                    'amp':1,}
        
        # Create DDS Output objects
        dds_prop = {}
        for i in range(1,self.num_AOM+1):
            dds_prop[f"CH-{i}"] = {"gate":{}}
            for subchnl in ['freq', 'amp']:
                dds_prop[f"CH-{i}"][subchnl] = {'base_unit':self.base_units[subchnl], 
                                                'min':self.base_min[subchnl],
                                                'max':self.base_max[subchnl],
                                                'step':self.base_step[subchnl],
                                                'decimals':self.base_decimals[subchnl],
                                                }
        # Create the output objects    
        self.create_dds_outputs(dds_prop)
        # Create widgets for output objects
        dds_widgets,_,_ = self.auto_create_widgets()
        # and auto place the widgets in the UI
        self.auto_place_widgets(("DDS Outputs",dds_widgets))

        # Set the capabilities of this device
        self.supports_remote_value_check(True)
        self.supports_smart_programming(True)
        

    def initialise_workers(self):
        # Create and set the primary worker
        self.create_worker(
            "main_worker",
            "user_devices.TimeBaseAOMDriver.blacs_workers.TimeBaseWorker",
            {"hostname":self.hostname, "port":self.port, "num_AOM":self.num_AOM})
        self.primary_worker = "main_worker"

