from labscript import IntermediateDevice,Device,DigitalOut, StaticAnalogQuantity, set_passed_properties, LabscriptError, MHz
from user_devices.TimeBaseAOMDriver.unitconversions import dBm_to_W, Hz_to_MHz
import numpy as np

class TimeBaseAOM(Device):
    description = "AOM conrolled by TimeBase AOM driver"
    allowed_children = [StaticAnalogQuantity]
    # This device is an adaption similar to the StatisDDS class 
    # from labscript, for AOM channels of the TimeBase driver
    
    def __init__(
            self, name, parent_device, connection, 
            digital_gate=[], AM_offset=None, FM_deviation=None,
            **kwargs):

        Device.__init__(self,name,parent_device,connection, **kwargs)

        self.amp = StaticAnalogQuantity(
            f"{self.name}_ampl",self,"amp",
            limits=(self.parent_device.min_amp,self.parent_device.max_amp), 
            unit_conversion_class=dBm_to_W
        )
        self.freq= StaticAnalogQuantity(
            f"{self.name}_freq",self,"freq",
            limits=(self.parent_device.min_freq,self.parent_device.max_freq), 
            unit_conversion_class=Hz_to_MHz
        )
        self.AM_offset = StaticAnalogQuantity(f"{self.name}_amoffs",self,"-",limits=(-225,15)) # Maximum would be 25, but this is not recommended for proper operation
        self.FM_deviation = StaticAnalogQuantity(f"{self.name}_fmdev",self,"-")


        if len(digital_gate)>0:
            # AOM is off when input at TTL is logic high --> inverted, default is off (high)
            self.gate = DigitalOut(self.name+'_gate',*digital_gate, inverted=True, default_value=1) 

    # Enable and disable AOM via gate directly
    def enable(self,t):
        self.gate.enable(t)
    def disable(self,t):
        self.gate.disable(t)

    def setamp(self,value,units=None):
        self.amp.constant(value,units)
    def setfreq(self,value,units=None):
        self.freq.constant(value,units)
    def setAMoffset(self,value,units=None):
        self.AM_offset.constant(value,units)
    def setFMdeviation(self,value,units=None):
        if value not in self.parent_device.FM_deviations:
            raise LabscriptError(f"FM devivation of {self.name} is none of the allowed values.")
        self.FM_deviation.constant(value,units)



class TimeBaseFreqSweepTrigger(DigitalOut):
    description = "Trigger for Frequency sweeps in TimeBase AOM Driver"    
    
    def __init__(self, name, parent_device, connection, AOM_device, **kwargs):
        DigitalOut.__init__(self,name, parent_device, connection, inverted=False, **kwargs)
        self.AOM = AOM_device
        self.TimeBase = AOM_device.parent_device
        # if self.AOM.FM_deviation._static_value:
        #     raise LabscriptError(f"Device {self.AOM.name} cannot have both FM and Frequency sweeps enabled.")
        self.start_freq= StaticAnalogQuantity(f"{AOM_device.name}_swps", AOM_device, "-", limits=(self.TimeBase.min_freq,self.TimeBase.max_freq))
        self.stop_freq = StaticAnalogQuantity(f"{AOM_device.name}_swpp", AOM_device, "-", limits=(self.TimeBase.min_freq,self.TimeBase.max_freq))
        self.step_freq = StaticAnalogQuantity(f"{AOM_device.name}_swpf", AOM_device, "-", limits=(self.TimeBase.min_step,self.TimeBase.max_step))
        self.step_time = StaticAnalogQuantity(f"{AOM_device.name}_swpt", AOM_device, "-", limits=(self.TimeBase.min_sweep_time,self.TimeBase.max_sweep_time))
        self.sweep_mode= StaticAnalogQuantity(f"{AOM_device.name}_swpm", AOM_device, "-")

    def ramp(self,t, duration, initial, final, samplerate, mode=4):
        # SAMPLERATE IS NOT THE FREQUENCY STEP SIZE!
        # The ramp mode can be either sawtooth (4) or triangle (2).
        # In the current implementation only one single ramp command is allowed!! 
        # For going back to the initial frequency use self.go_low or self.disable. 
        # The ramp type determines if the frequency jumps back to the 
        # initial value (mode 4) or starts a downward ramp (mode 2).
        if mode not in (2,4):
            raise LabscriptError(f"Ramp mode of device {self.name} for frequency sweep can only be 2 (triangle) or 4 (sawtooth).")
        if not initial == self.AOM.freq.static_value:
            raise LabscriptError(f"Start frequency {initial} of {self.name} must be equal to AOM freuquency ({self.AOM.freq.static_value})")
        # Set TimeBase sweep parameters
        self.start_freq.constant(initial)
        self.stop_freq.constant(final)
        self.step_freq.constant(abs(final-initial)/duration/samplerate)
        self.step_time.constant(1/samplerate*1e9) # in ns
        self.sweep_mode.constant(mode)
        # Set trigger output
        self.enable(t)
        return duration

    

        


class TimeBaseAOMDriver(IntermediateDevice):
    description = "TimeBase ADRV-5 AOM driver with 5 channels"
    allowed_children = [TimeBaseAOM]
    # Capabilities
    min_freq = 10*MHz
    max_freq = 400*MHz
    min_amp = 14 # dBm
    max_amp = 34 # dBm
    FM_deviations = 3200 * 1<<np.arange(16)
    max_step = 100*MHz
    min_step = 1
    max_sweep_time = 262140 # 262 140 ns
    min_sweep_time = 4      # 4 ns

    @set_passed_properties(
        property_names={
            "connection_table_properties":
            ["hostname","port","num_AOM","min_freq","max_freq","min_amp","max_amp","FM_deviations","max_step","min_step","max_sweep_time","min_sweep_time"]
    })
    def __init__(self, name, hostname, port=8081, num_AOM=5, **kwargs):
        super().__init__(name, parent_device=None, **kwargs)
        self.BLACS_connection = hostname
        self.num_AOM = num_AOM


    def generate_code(self, hdf5_file):
        group = hdf5_file.create_group(f"devices/{self.name}")

        for AOM in self.child_devices:
            AOM_group = group.create_group(AOM.connection)
            AOM_group.attrs["Sswpm"] = 0 # Always turn off sweep mode, if not used.
            
            # Set all commands for the TimeBase Driver as hdf5 Attributes
            for output in AOM.get_all_outputs():
                command = "S" + output.name.rsplit("_",1)[-1]
                if output._static_value is not None:
                    AOM_group.attrs[command] = np.round(output.static_value).astype(np.int32)
            
            # Amplitude has to be programmed in 0.1*dBm
            if "Sampl" in AOM_group.attrs:
                AOM_group.attrs["Sampl"] *= 10

            # Set if FM is enabled and check the not FM and Sweep are used at the same time
            if "Sfmdev" in AOM_group.attrs:
                AOM_group.attrs["Sfmon"] = 1
                if AOM_group.attrs["Sswpm"] != 0:
                    raise LabscriptError(f"Device {AOM.name} cannot have both FM and Frequency sweeps enabled.")
            else:
                AOM_group.attrs["Sfmon"] = 0

        super().generate_code(hdf5_file)

        
