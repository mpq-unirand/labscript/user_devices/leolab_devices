'<ADbasic Header, Headerversion 001.001>
' Process_Number                 = 1
' Initial_Processdelay           = 2000
' Eventsource                    = Timer
' Control_long_Delays_for_Stop   = No
' Priority                       = High
' Version                        = 1
' ADbasic_Version                = 6.3.1
' Optimize                       = Yes
' Optimize_Level                 = 4
' Stacksize                      = 1000
' Info_Last_Save                 = MPQ6P176  MPQ6P176\UniRand PC
'<Header End>
#include adwinpro_all.inc

' Array Size Settings
' TODO: check these values in labscript_devices files
#define ZERO            32768       ' Zero Volts in Adwin 16bit representation
#define N               32768       ' Max Voltage offset by ZERO
#define MAX_EVENTS      200000      ' Max number of events that can be stored
#define MAX_PID_EVENTS  2000        ' Max number of changing the AIN channel for PID feedback to AOUT
#define MAX_TICO_EVENTS 3000        ' Max number of Digital Output events
#define A_IN_BUFFER     30000000    ' Size of Array to transmit AIN values to the runner PC
#define PIDNO           100         ' Max Number of PIDs
#define AOUTNO          16          ' Number of output channels
#define AINNO           16          ' Number of input channels

' Set Module Adresses
' TODO get or set these values from labscript ?
#define DIO1  1                         'DIO  Card 32
#define DIO2  2                         'DIO  Card 32
#define AIN1  3                         'AIN  Card 8/16
#define AIN2  4                         'AIN  Card 8/16
#define AOUT1 5                         'AOUT Card 8/16
#define AOUT2 6                         'AOUT Card 8/16

' Declare arrays for analog data from experiment control software
DIM DATA_1[MAX_EVENTS] AS LONG          'Analog time
DIM DATA_2[MAX_EVENTS] AS LONG          'Analog channel
DIM DATA_3[MAX_EVENTS] AS LONG          'Analog ouput value

' Declare arrays for PID channels for each AOUT channel
DIM DATA_4[MAX_PID_EVENTS] AS LONG      'Times for changing PID channels
DIM DATA_5[MAX_PID_EVENTS] AS LONG      'PID A_OUT channel
DIM DATA_6[MAX_PID_EVENTS] AS LONG      'PID A_In channel
DIM DATA_25[MAX_PID_EVENTS] AS FLOAT         'PID P values
DIM DATA_26[MAX_PID_EVENTS] AS FLOAT         'PID I values
DIM DATA_27[MAX_PID_EVENTS] AS FLOAT         'PID D values
DIM DATA_28[MAX_PID_EVENTS] AS LONG         'PID min values
DIM DATA_29[MAX_PID_EVENTS] AS LONG         'PID max values

' Declare arrays for AIN Channel Timings
DIM DATA_7[AINNO] AS LONG               'Start times for channel recording
DIM DATA_8[AINNO] AS LONG               'End times for channel recording
DIM DATA_9[AINNO] AS LONG               'Gain modes for AIN channels
DIM DATA_99[30000016] AS LONG     'Recorded Data - add padding at the end to ensure no overflow happens

'Declare arrays for digital data from experiment control software
DIM DATA_10[MAX_TICO_EVENTS] AS LONG    'DIO1 TiCo times
DIM DATA_11[MAX_TICO_EVENTS] AS LONG    'DIO1 digital output values
DIM DATA_20[MAX_TICO_EVENTS] AS LONG    'DIO2 TiCo times
DIM DATA_21[MAX_TICO_EVENTS] AS LONG    'DIO2 digital output values

' TO BE REMOVED IN REAL USAGE !!
DIM DATA_31[MAX_EVENTS] AS LONG ' Workload Array for testing

'Declare arrays to store TICO memory pointers
DIM PTR_TICO1[150] AS LONG
DIM PTR_TICO2[150] AS LONG

'Declare PID SETTINGS Arrays
DIM pid_P[PIDNO] AS FLOAT
DIM pid_I[PIDNO] AS FLOAT
DIM pid_D[PIDNO] AS FLOAT
DIM pid_min[PIDNO] AS LONG
DIM pid_max[PIDNO] AS LONG

'Declare PID integrator and differentiator values
DIM pidn AS LONG 'Number of selected PID channels
DIM pid_error AS LONG 'Difference between target and actual voltage
DIM pid_dError AS FLOAT 'Difference times differential gain
DIM pid_lin AS FLOAT 'Proportional part of PID
DIM pid_sum[AOUTNO] AS FLOAT 'Integral part of PID
DIM pid_diff AS FLOAT 'Differential part of PID
DIM pid_prev_dError[AOUTNO] AS FLOAT


'Declare other variables
DIM processIdx AS LONG
DIM eventIdx AS LONG
DIM pidIdx AS LONG
DIM aInIdx AS LONG
DIM nextPidTime AS LONG
DIM nextAInTime AS LONG
DIM timer AS LONG 'timing the event duration
DIM i AS LONG 'for loop index

'Declare arrays for storing AIN and setting AOUT channels
DIM set_target[AOUTNO] AS LONG  ' Set values to be written to AOUTs
DIM set_output[AOUTNO] AS LONG  ' Set values to be written to AOUTs
DIM set_pid[AOUTNO] AS LONG    ' PID selection for each AOUT
DIM act_values[PIDNO] AS LONG   ' Analog values measured from AINs


'========================================================================================================================================
'= INIT             =
'========================================================================================================================================

init:
  'Set Processdelay to 2us
  PROCESSDELAY = 2000
  par_1 = 1 'Process Status

  'Initialize Variables
  processIdx = 0
  eventIdx = 0
  pidIdx = 0
  aInIdx = 0
  nextPidTime = DATA_4[1]
  nextAInTime = DATA_1[1]

  '===========================================================INITIALIZE TICOS============================================================
  'Set DIO channels as outputs
  P2_DigProg(DIO1, 1111b)     'Channel 0-31 as outputs
  P2_DigProg(DIO2, 1111b)     'Channel 32-63 as outputs

  'Initialize pointers to Tico memory
  P2_TiCo_Reset(000011b)
  P2_TDrv_Init(DIO1, 1, PTR_TICO1)    'Data Transfer to Module DIO1
  P2_TDrv_Init(DIO2, 1, PTR_TICO2)    'Data Transfer Module DIO2

  'Load digital event data to tico 1
  P2_SetData_Long(PTR_TICO1, 1, 1, MAX_TICO_EVENTS, DATA_10, 1, 1) 'DIO1 TiCo times
  P2_SetData_Long(PTR_TICO1, 2, 1, MAX_TICO_EVENTS, DATA_11, 1, 1) 'DIO1 digital output values

  'Load digital event data to tico 2
  P2_SetData_Long(PTR_TICO2, 1, 1, MAX_TICO_EVENTS, DATA_20, 1, 1) 'DIO2 TiCo times
  P2_SetData_Long(PTR_TICO2, 2, 1, MAX_TICO_EVENTS, DATA_21, 1, 1) 'DIO2 digital output values

  'Initialize Digital Channels
  P2_Set_Par(DIO1,1,1,DATA_11[1])   'DIO1 digital output values
  P2_Set_Par(DIO2,1,1,DATA_21[1])   'DIO2 digital output values

  'Initialize time counter for tico to synchronize to analog channels
  P2_Set_Par(DIO1,1,2,81)
  P2_Set_Par(DIO2,1,2,81)

  'Initialize event counter for digital channels
  P2_Set_Par(DIO1,1,3,2)
  P2_Set_Par(DIO2,1,3,2)

  '=============================================================PID SETTINGS==============================================================

  'Initialize Integrator and Difference
  FOR i=1 TO AOUTNO
    pid_sum[i]=0
    pid_prev_dError[i]=0
  NEXT i

  ' Set paramters of manual PID control from GUI
  IF (Par_21 <> 0) THEN
    pid_P[Par_21] = FPar_21
    pid_I[Par_21] = FPar_22
    pid_D[Par_21] = FPar_23
  ENDIF

  '===========================================================INITIALIZE AOUTS============================================================
  'Initialize set values of all output channels to 0V
  pid_min[0] = -N
  pid_max[0] = 65535
  FOR i=1 TO AOUTNO
    set_target[i]=ZERO
    set_output[i]=ZERO
    act_values[i]=ZERO
    set_pid[i]=0
    pid_P[i]=0
    pid_I[i]=0
    pid_D[i]=0
    pid_min[i]=-N
    pid_max[i]=65535
  NEXT i

  'Write analog out values
  P2_Write_DAC8(AOUT1,set_output,1)
  P2_Write_DAC8(AOUT2,set_output,9)
  'Output set voltages
  P2_Start_DAC(AOUT1)
  P2_Start_DAC(AOUT2)

  '============================================================OTHER SETTINGS=============================================================
  'Set DIO of T12 as input
  CPU_Dig_IO_Config(10b)

  'Turn LEDs ON
  P2_Set_Led(DIO1, 1)
  P2_Set_Led(DIO2, 1)
  P2_Set_Led(AIN1, 1)
  P2_Set_Led(AIN2, 1)
  P2_Set_Led(AOUT1, 1)
  P2_Set_Led(AOUT2, 1)

  '==========================================================AIN CONFIGURATION===========================================================

  'Set P2_ADCF_Mode - leave it set standard (=0) here since later the sampling rate is set by hand
  P2_ADCF_Mode(2^(AIN1-1),0)
  'Average over 4 sample values
  P2_Set_Average_Filter(AIN1,2)
  'Set P2_ADCF_Mode - leave it set standard (=0) here since later the sampling rate is set by hand
  P2_ADCF_Mode(2^(AIN2-1),0)
  'Average over 4 sample values
  P2_Set_Average_Filter(AIN2,2)

  'Set Gain of AIN Channels
  FOR i=1 TO 8
    P2_Set_Gain(AIN1,i,DATA_9[i])
    P2_Set_Gain(AIN2,i,DATA_9[i+8])
  NEXT i


  '### Set the Sampling Rate of the AIN cards. The Period is given by T = 250ns + (value-1)* 10ns. The value is set by writing to the correct register using POKE(register,value)
  CHECK_CARD(AIN1)
  POKE((68050100h OR  shift_left(AIN1,20)),1)

  CHECK_CARD(AIN2)
  POKE((68050100h OR  shift_left(AIN2,20)),1)

  'Start Conversion for first event
  P2_Sync_All(001100b)
  'Wait for 200 * 10ns to make sure that first ADC values are ready
  CPU_Sleep(200) ' TODO: Do we still need to wait if we set DIOs after Sync All?

  ' Set first output values at DIOs
  P2_DIGOUT_LONG(DIO1,DATA_11[1])
  P2_DIGOUT_LONG(DIO2,DATA_21[1])

  '========================================================================================================================================
  '=                                                        EVENT LOOP                                                                   =
  '========================================================================================================================================

EVENT:
  timer = Read_Timer_Sync()
  'Synchronize DIO Channels
  if(processIdx = 20) then
    P2_set_par(DIO1,1,20,1)    'starte tico event Modul 1
    P2_set_par(DIO2,1,20,1)    'starte tico event Modul 5
  endif

  '======================================================== READ ANALOG IN CARDS ========================================================

  'Read act_values at all ADC Channels
  P2_READ_ADCF8(AIN1, act_values, 1)    'READ CHANNEL 1-8 OF AIN MODULE 1
  P2_READ_ADCF8(AIN2, act_values, 9)   'READ CHANNEL 1-8 OF AIN MODULE 2

  'Start conversion at all ADC Channels Synced for next Event
  P2_Sync_All(001100b)

  '=======================================================CALCULATE NEW SET VALUES =======================================================

  FOR i=1 TO AOUTNO
    pidn = set_pid[i]
    if (pidn = 0) then
      'No PID active
      set_output[i]=set_target[i]
    else
      'Get deviation from target voltage
      pid_error = set_target[i]- act_values[pidn]
      'Get proportional part
      pid_lin=pid_P[pidn]*pid_error
      'Get integral part
      pid_sum[i] = pid_sum[i] + pid_I[pidn]*pid_error
      'Get differential part
      pid_dError = pid_D[pidn]* pid_error
      pid_diff = pid_dError - pid_prev_dError[i]
      pid_prev_dError[i] = pid_dError

      'Check for integral overflow
      if (pid_sum[i] > N) then pid_sum[i] = N
      if (pid_sum[i] < -N) then pid_sum[i] = -N

      set_output[i]=(pid_lin+pid_sum[i]+pid_diff) + N
    endif
    ' Check that output is within safe limits
    if (set_output[i] < pid_min[pidn]) then set_output[i] = pid_min[pidn]
    if (set_output[i] > pid_max[pidn]) then set_output[i] = pid_max[pidn]
    ' Check that output is within safe limits [-10,+10]V
    if (set_output[i] < -N) then set_output[i] = - N
    if (set_output[i] > 65535) then set_output[i] = 65535
  NEXT i


  '======================================================== SET NEW AOUT VALUES ========================================================
  'Write values to AOUT Cards
  P2_Write_DAC8(AOUT1,set_output,1)  ''sende neue stellwerte an die Analog out Karten
  P2_Write_DAC8(AOUT2,set_output,9)

  'Set all voltages synchronously
  P2_Sync_All(110000b)

  inc processIdx

  ' Write ADC Values
  IF (aInIdx < A_IN_BUFFER) THEN
    FOR i=1 to AINNO
      IF(processIdx <= DATA_8[i]) THEN
        IF (processIdx > DATA_7[i]) THEN
          inc aInIdx
          DATA_99[aInIdx]=act_values[i]
        ENDIF 'processIdx > DATA_7[i]
      ENDIF 'processIdx <= DATA_8[i]
    NEXT i
  ENDIF 'aInIdx < A_IN_BUFFER


  '### Programmterminierung
  IF (processIdx  >= par_2) THEN
    END 'end loop when last last timing event completed
  ENDIF

  ' Update target values
  IF(nextAInTime <= processIdx) THEN
    DO
      inc eventIdx
      set_target[DATA_2[eventIdx]]=DATA_3[eventIDX]
    UNTIL(DATA_1[eventIdx+1] > processIdx)
    nextAInTime = DATA_1[eventIdx+1]
  ENDIF

  ' Update pid target
  IF(nextPidTime <= processIdx) Then
    DO
      inc pidIdx
      set_pid[DATA_5[pidIdx]] = DATA_6[pidIdx]
      pid_sum[DATA_5[pidIdx]] = 0 'act_values[DATA_6[pidIdx]]-N
      pid_prev_dError[DATA_5[pidIdx]] = 0
      pid_P[DATA_6[pidIdx]] = DATA_25[pidIdx]
      pid_I[DATA_6[pidIdx]] = DATA_26[pidIdx]
      pid_D[DATA_6[pidIdx]] = DATA_27[pidIdx]
      pid_min[DATA_6[pidIdx]] = DATA_28[pidIdx]
      pid_max[DATA_6[pidIdx]] = DATA_29[pidIdx]
    UNTIL(DATA_4[pidIdx+1] > processIdx)
    nextPidTime = DATA_4[pidIdx+1]
  ENDIF

  '### Debug Timer
  par_13=READ_TIMER_SYNC() - timer

  ' TO BE REMOVED IN REAL USAGE !!
  IF (processIdx<MAX_EVENTS) THEN
    DATA_31[processIdx] = par_13
  ENDIF

  par_15=processIdx

FINISH:

  'Turn Module LEDs OFF
  P2_Set_Led(DIO1,0)
  P2_Set_Led(DIO2,0)
  P2_Set_Led(AIN1,0)
  P2_Set_Led(AIN2,0)
  P2_Set_Led(AOUT1,0)
  P2_Set_Led(AOUT2,0)

  ' Check if TiCo Status was fine
  par_10 = P2_Process_Status(DIO1,1,1)
  par_20 = P2_Process_Status(DIO2,1,1)
  'Stop Tico Processes
  P2_TiCo_Stop_Process(PTR_TICO1,1)
  P2_TiCo_Stop_Process(PTR_TICO2,1)

  'Set values of all output channels to 0V
  for i=1 to AOUTNO
    set_output[i]=ZERO
  next i

  'Write analog out values
  P2_Write_DAC8(AOUT1,set_output,1)
  P2_Write_DAC8(AOUT2,set_output,9)
  'Output set voltages
  P2_Start_DAC(AOUT1)
  P2_Start_DAC(AOUT2)

  PAR_1 = 0
