'<ADbasic Header, Headerversion 001.001>
' Process_Number                 = 2
' Initial_Processdelay           = 10000
' Eventsource                    = Timer
' Control_long_Delays_for_Stop   = No
' Priority                       = High
' Version                        = 1
' ADbasic_Version                = 6.3.1
' Optimize                       = Yes
' Optimize_Level                 = 1
' Stacksize                      = 1000
' Info_Last_Save                 = MPQ6P176  MPQ6P176\UniRand PC
'<Header End>
#include adwinpro_all.inc

#define ZERO            32768       ' Zero Volts in Adwin 16bit representation
#define N               32768       ' Max Voltage offset by ZERO

#define DIO1  1                         'DIO  Card 32
#define DIO2  2                         'DIO  Card 32
#define AIN1  3                         'AIN  Card 8/16
#define AIN2  4                         'AIN  Card 8/16
#define AOUT1 5                         'AOUT Card 8/16
#define AOUT2 6                         'AOUT Card 8/16

#define AOUTNO          16          ' Number of output channels
#define PIDNO           100         ' Max Number of PIDs


DIM DATA_95[8] AS Long ' AOUT1 values
DIM DATA_96[8] AS Long ' AOUT2 values
DIM DATA_93[8] AS Long ' AIN1 values in FiFo array
DIM DATA_94[8] AS Long ' AIN2 values in FiFo array

DIM DATA_97[16] AS LONG      'PID A_OUT channel
DIM DATA_98[16] AS LONG      'PID A_In channel
DIM DATA_101[16] AS LONG     'Setting PID channels
DIM DATA_102[16] AS LONG     'Min PID value
DIM DATA_103[16] AS LONG     'Max PID value

'Declare PID SETTINGS Arrays
DIM pid_P[PIDNO] AS FLOAT
DIM pid_I[PIDNO] AS FLOAT
DIM pid_D[PIDNO] AS FLOAT

'Declare PID integrator and differentiator values
DIM pidn AS LONG 'Number of selected PID channels
DIM pid_error AS LONG 'Difference between target and actual voltage
DIM pid_dError AS FLOAT 'Difference times differential gain
DIM pid_lin AS FLOAT 'Proportional part of PID
DIM pid_sum[AOUTNO] AS FLOAT 'Integral part of PID
DIM pid_diff AS FLOAT 'Differential part of PID
DIM pid_prev_dError[AOUTNO] AS FLOAT

'Declare arrays for storing AIN and setting AOUT channels
DIM set_target[AOUTNO] AS LONG  ' Set values to be written to AOUTs
DIM set_output[AOUTNO] AS LONG  ' Set values to be written to AOUTs
DIM set_pid[AOUTNO] AS LONG    ' PID selection for each AOUT
DIM act_values[PIDNO] AS LONG   ' Analog values measured from AINs

DIM i AS LONG 'for loop index

INIT:
  PROCESSDELAY = 10000 ' in manual mode 1 ms cycle time is enough
  Par_11 = 0 ' Parameter to start the output
  P2_Set_Led(DIO1, 1)
  P2_Set_Led(DIO2, 1)
  P2_Set_Led(AOUT1, 1)
  P2_Set_Led(AOUT2, 1)
  P2_Start_ConvF(AIN1,0FFh)
  P2_Start_ConvF(AIN2,0FFh)

  '=============================================================PID SETTINGS==============================================================
  'Initial setup for all PID channels. You may override this if these parameters don't work out for you.
  FOR i=1 TO AOUTNO
    pid_P[i] = 0.01
    pid_I[i] = 0.01
    pid_D[i] = 0
  NEXT i

  FOR i=1 TO 16
    DATA_101[i]=0
    DATA_102[i]=-N
    DATA_103[i]=65535
  NEXT i


  'Initialize Integrator and Difference
  FOR i=1 TO AOUTNO
    pid_sum[i]=0
    pid_prev_dError[i]=0
  NEXT i

EVENT:
  IF (Par_11=1) THEN
    P2_Wait_EOCF(AIN1,0FFh)
    P2_Wait_EOCF(AIN2,0FFh)
    P2_READ_ADCF8(AIN1, DATA_93, 1)
    P2_READ_ADCF8(AIN2, DATA_94, 1)
    P2_Start_ConvF(AIN1,0FFh)
    P2_Start_ConvF(AIN2,0FFh)


    'TODO: Combine DATA_93 and 94. For now PID loops are only possible on AIN1 / AOUT1

    FOR i=1 TO 8
      set_output[i] = DATA_95[i]
    NEXT i
    FOR i=9 TO 16
      set_output[i] = DATA_96[i]
    NEXT i


    FOR i=1 TO 8
      pidn = DATA_101[i]
      if (pidn = 0) then
        'No PID active
        set_output[i]=set_target[i]
        pid_sum[i] = DATA_93[pidn]-N
        pid_prev_dError[i] = 0
      else
        'Get deviation from target voltage
        pid_error = set_target[i]- DATA_93[pidn]
        'Get proportional part
        pid_lin=pid_P[pidn]*pid_error
        'Get integral part
        pid_sum[i] = pid_sum[i] + pid_I[pidn]*pid_error
        'Get differential part
        pid_dError = pid_D[pidn]* pid_error
        pid_diff = pid_dError - pid_prev_dError[i]
        pid_prev_dError[i] = pid_dError

        'Check for integral overflow
        if (pid_sum[i] > N) then pid_sum[i] = N
        if (pid_sum[i] < -N) then pid_sum[i] = -N

        set_output[i]=(pid_lin+pid_sum[i]+pid_diff) + N
      endif
      ' Check that output is within safe limits
      if (set_output[i] < DATA_102[i]) then set_output[i] = DATA_102[i]
      if (set_output[i] > DATA_103[i]) then set_output[i] = DATA_103[i]
      if (set_output[i] < - N) then set_output[i] = - N
      if (set_output[i] > 65535) then set_output[i] = 65535
    NEXT i

    FOR i=9 TO 16
      pidn = DATA_101[i]
      if (pidn = 0) then
        'No PID active
        set_output[i]=set_target[i]
        pid_sum[i] = DATA_94[pidn]-N
        pid_prev_dError[i] = 0
      else
        'Get deviation from target voltage
        pid_error = set_target[i]- DATA_94[pidn]
        'Get proportional part
        pid_lin=pid_P[pidn]*pid_error
        'Get integral part
        pid_sum[i] = pid_sum[i] + pid_I[pidn]*pid_error
        'Get differential part
        pid_dError = pid_D[pidn]* pid_error
        pid_diff = pid_dError - pid_prev_dError[i]
        pid_prev_dError[i] = pid_dError

        'Check for integral overflow
        if (pid_sum[i] > N) then pid_sum[i] = N
        if (pid_sum[i] < -N) then pid_sum[i] = -N

        set_output[i]=(pid_lin+pid_sum[i]+pid_diff) + N
      endif
      ' Check that output is within safe limits
      if (set_output[i] < DATA_102[i]) then set_output[i] = DATA_102[i]
      if (set_output[i] > DATA_103[i]) then set_output[i] = DATA_103[i]
      if (set_output[i] < - N) then set_output[i] = - N
      if (set_output[i] > 65535) then set_output[i] = 65535
    NEXT i




    P2_DAC8(AOUT1,set_output,1)
    P2_DAC8(AOUT2,set_output,9)
    P2_DIGOUT_LONG(DIO1,Par_91)
    P2_DIGOUT_LONG(DIO2,Par_92)

    FOR i=1 TO 8
      set_target[i] = DATA_95[i]
      set_pid[i] = DATA_98[i]
    NEXT i
    FOR i=9 TO 16
      set_target[i] = DATA_96[i]
      set_pid[i] = DATA_98[i]
    NEXT i

  ENDIF

FINISH:
  P2_SET_LED(DIO1,0)
  P2_SET_LED(DIO2,0)
  P2_SET_LED(AOUT1,0)
  P2_SET_LED(AOUT2,0)
  P2_SET_LED(AIN1,0)
  P2_SET_LED(AIN2,0)