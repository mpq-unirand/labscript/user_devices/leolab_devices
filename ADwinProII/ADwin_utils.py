#####################################################################
#                                                                   #
# ADwinProII/ADwin_utils.py                                         #
#                                                                   #
# Copyright 2022, TU Vienna                                         #
#                                                                   #
# Implementation of the ADwin-Pro II for the labscript-suite,       #
# used in the Léonard lab for Experimental Quantum Information.     #
#                                                                   #
#####################################################################

import numpy as np

def ADC(voltage,resolution=16,min_V=-10,max_V=10):
    """
    Convert analog voltage values to the digital format for ADwin AOut/AIn modules.
    The 16 bits (resolution) cover the range of [-10,10) Volts, with 0 := -10V.
    The step size therefore is 0.305mV and 32768 is used for 0V.
    """
    output = (voltage-min_V)/(max_V-min_V)*(1<<resolution)
    output = np.round(output).astype(np.int32)
    # The maximum digital value is 65535 (= 9.999695V).
    # For 10V the value is rounded to 65536, which is equivalent to -10V.
    # We correct this rounding error here by hand:
    if np.any(output==65536):
        if isinstance(output,np.ndarray):
            output[output==65536] = 65535
        elif isinstance(output,float) or isinstance(output,np.uint) or isinstance(output,np.int32):
            output = 65536
        else:
            raise NotImplementedError
    return output

def DAC(values,resolution=16,min_V=-10,max_V=10):
    """
    Convert the digital representation in ADwin to the analog voltage.
    The 16 bits (resolution) cover the range of [-10,10) Volts, with 0 := -10V.
    The step size therefore is 0.305mV and 32768 is used for 0V.  
    """
    return (values)*(max_V-min_V)/(1<<resolution) + min_V

def get_channel_from_BLACS_name(string):
    """
    Helper function to split the module name and channel number from the 
    BLACS hardware name.
    Format: e.g. "DIO32_1/1"
    --------------------------
    Returns: channel
        Integer number of the output/input channel.
    """
    card,channel = string.split('/')
    return int(channel)

def get_aout_trace(h5file,name):
    """
    Function to return output trace [s],[V] converted from the ADwin instruction array in the h5file.
    """
    from labscript_utils import connections
    import h5py
    conn_table = connections.ConnectionTable(h5file)
    conn = conn_table.find_by_name(name)
    port = int(conn.parent_port) + conn.parent.properties["start_index"]
    PROCESSDELAY = conn_table.find_by_name("ADwin").properties["PROCESSDELAY"]
    if port is None:
        return None
    with h5py.File(h5file, 'r') as f:
        data = f["devices/ADwin/ANALOG_OUT/VALUES"][:]
        mask = data["channel"]==int(port)
        t = np.round(data["n_cycles"][mask] * 1e-9 * PROCESSDELAY, 9)
        values = DAC(data["value"][mask])
    return t, values