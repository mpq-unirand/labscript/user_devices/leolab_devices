############################################################################
#                                                                          #
# ADwinProII/__init__.py                                                   #
#                                                                          #
# Copyright 2022, TU Vienna                                                #
#                                                                          #
# Implementation of the ADwin-Pro II for the labscript-suite,              #
# used in the Léonard lab for Experimental Quantum Information.            #
#                                                                          #
# Some parts of the code were adapted from an older implementation         #
# in the labscript-suite:                                                  # 
# https://github.com/labscript-suite/labscript/blob/2.1.0/devices/adwin.py #
#                                                                          #
############################################################################


from labscript import Pseudoclock, PseudoclockDevice, ClockLine, LabscriptError, config, set_passed_properties
import numpy as np

from user_devices.ADwinProII import PROCESSDELAY_T12, PROCESSDELAY_TiCo, CLOCK_T12, CLOCK_TiCo 
from user_devices.ADwinProII.labscript_devices_ADwin_modules import _ADwinCard,ADwinAI8,ADwinAO8,ADwinDIO32
from user_devices.ADwinProII.ADwin_utils import ADC

# Notes:
# The ADWin (T12) runs at 1 GHz. The cycle time should be specified in hardware programming in units of this clock speed.
# Subsequently, instruction timing must be specified in units of cycles.

# Voltages are specified with a 16 bit unsigned integer, mapping the range [-10,10) volts.
# There are 32 digital outputs on a card (DIO-32-TiCo)
# There are 8 analog outputs on a card (AOut-8/16)
# There are 8 analog inputs on a card (AIn-F-8/16)


class _ADwin_CPU(Pseudoclock):
    """Dummy Class to create the pseudoclock for a ADwin processor with a given PROCESSDELAY"""
    CPU_clock = None

    def __init__(self, name, pseudoclock_device, connection, PROCESSDELAY, **kwargs):
        super().__init__(name, pseudoclock_device, connection, **kwargs)
        self.clock_limit = self.CPU_clock / PROCESSDELAY
        self.clock_resolution = 1/self.clock_limit

    def add_device(self, device):
        if self.child_devices or not isinstance(device,ClockLine):
            raise LabscriptError("The ADwin CPU only supports one clockline, which is generated automatically.")
        Pseudoclock.add_device(self, device)
        self.clockline = device
        self.clockline.allowed_children = [_ADwinCard]



class _ADwin_CPU_T12(_ADwin_CPU):
    description = "ADwin-Pro 2 T12 Processor "
    CPU_clock = CLOCK_T12

    
class _ADwin_CPU_TiCo(_ADwin_CPU):
    description = "TiCo1 Processor."
    CPU_clock = CLOCK_TiCo

        

class ADwinProII(PseudoclockDevice):
    description = 'ADWin-Pro II'
    CPU_clock_T12 = CLOCK_T12
    allowed_children = [_ADwin_CPU_T12,_ADwin_CPU_TiCo]
    TiCos = {}

    @set_passed_properties(
        property_names={
            "connection_table_properties": ["device_no","PROCESSDELAY","process_buffered","process_manual"],
            "device_properties": [],
        }
    )
    def __init__(self,name="adwin",device_no=1,process_buffered="",process_manual="",PROCESSDELAY=PROCESSDELAY_T12,**kwargs):
        PseudoclockDevice.__init__(self, name, None, None, **kwargs)
        self.BLACS_connection = name + "_" + str(device_no)
        self.process_buffered = str(process_buffered)
        self.process_manual = str(process_manual)
        
        # Set a lower bound for the clock limit euqual to the CPU rate of the
        # T12 processor. The actual clock limits are then specified with the
        # PROCESSDELAY in the Pseudoclocks for the T12 and each TiCo processors.
        # This value is necessary, because we have clocks with different cycle 
        # times in the ADwin (T12 and TiCo), and the clock resolution here has 
        # to be smaller than both of them.
        self.clock_limit = self.CPU_clock_T12
        self.clock_resolution = 1/self.clock_limit
        self.PROCESSDELAY = PROCESSDELAY

        self._pseudoclock_T12 = _ADwin_CPU_T12(
            name=f'{name}_T12',
            pseudoclock_device=self,
            connection='internal',
            PROCESSDELAY=PROCESSDELAY_T12
        )
        self._clockline_T12 = ClockLine(
            name=f'{name}_clockline_T12',
            pseudoclock=self._pseudoclock_T12,
            connection='internal'
        )

    @property
    def pseudoclock_T12(self):
        return self._pseudoclock_T12
    
    @property
    def clockline_T12(self):
        return self._clockline_T12

    def add_module(self,ClassConstructor,name, module_address, **kwargs):
        """ This function creates a _ADwinCard device (e.g. ADwinAO8, ADwinDIO32) and 
        adds it as a child of the proper ADwin clockline and to the experiment logic.
        We can't call the module's Class constructor in the connection_table directly,
        becasue this requires the module to be registerd as a labscript device (in 
        register_classes.py) and having its own BLACS Tab/Worker (we control the whole 
        ADwin from one Tab/Worker."""
        ClassConstructor(name=name,parent_device=self,module_address=module_address,**kwargs)

    def add_TiCo(self, device_name, module_address, PROCESSDELAY):
        """Adding a new TiCo pseudoclock and clockline, for DIO32-TiCo modules"""
        self.TiCos[device_name] = _ADwin_CPU_TiCo(
            name = f"{device_name}_TiCo",
            pseudoclock_device=self,
            connection=module_address,
            PROCESSDELAY=PROCESSDELAY
        )
        ClockLine(
            name = f"{device_name}_clockline_TiCo",
            pseudoclock=self.TiCos[device_name],
            connection="internal"
        )
        return self.TiCos[device_name]
    
    def do_checks(self, outputs):
        if self.trigger_times != [0]:
            raise LabscriptError('ADWin does not support retriggering or waiting.')
            # TODO: check if this really is true, ADwin could be triggered by "EVENT IN" signal
        for output in outputs:
            output.do_checks(self.trigger_times)

    def collect_all_modules(self):
        """ Returns a list of all ADwin-Pro modules sorted by the module address."""
        modules = []
        for child in self.child_devices:
            if isinstance(child,_ADwin_CPU_T12) or isinstance(child,_ADwin_CPU_TiCo):
                for device in child.clockline.child_devices:
                    modules.append(device)
        self.modules = sorted(modules, key=lambda mod:int(mod.module_address))

    def collect_card_instructions(self, hdf5_file):
        group = hdf5_file.require_group('/devices/%s'%self.name)
        AO_group = group.require_group('ANALOG_OUT')
        AI_group = group.require_group('ANALOG_IN')

        analog_output = np.array([])
        PID_channels = np.array([])
        analog_input = np.array([])
 
        for device in self.modules:
            if isinstance(device,ADwinAO8):
                analog_output = np.hstack([analog_output, device.outputs]) if analog_output.size else device.outputs
                PID_channels = np.hstack([PID_channels, device.PID_channels]) if PID_channels.size else device.PID_channels
            elif isinstance(device,ADwinDIO32):
                group.create_dataset("DIGITAL_OUT/"+device.name, data=device.digital_data)
            elif isinstance(device,ADwinAI8):
                analog_input = np.hstack([analog_input, device.AIN_times]) if analog_input.size else device.AIN_times
            else:
                raise AssertionError(f"Invalid child device {device.name} of type {type(device).__name__}, shouldn't be possible")
        
        # Sort and store analog outputs and PID channels
        analog_output = np.sort(analog_output, axis=0)
        PID_channels = np.sort(PID_channels, axis=0)
        # Add one last line to PID and AOut arrays 
        last_values = self.stop_time*self._pseudoclock_T12.clock_limit + 1
        PID_channels = np.concatenate([PID_channels,np.full(1,last_values,dtype=PID_channels.dtype)])
        analog_output = np.concatenate([analog_output,np.full(1,last_values,dtype=analog_output.dtype)])
        # Save datasets
        AO_group.create_dataset('VALUES', compression=config.compression, data=analog_output)
        AO_group.create_dataset('PID_CHANNELS', compression=config.compression, data=PID_channels)
        AI_group.create_dataset('TIMES', data=analog_input)
        AI_group.attrs["AIN_count"] = np.sum(analog_input["stop_time"] - analog_input["start_time"])

    
    def generate_code(self, hdf5_file):
        self.collect_all_modules()
        outputs = self.get_all_outputs()      

        # These functions do some checks and make sure the timings are rounded to a value
        # that the clocks can handle (even if there is no trigger offset).
        self.do_checks(outputs)
        self.offset_instructions_from_trigger(outputs)

        # We don't need to call generate_code for the Pseudoclocks, because for the
        # outputs we only need the list when each output changes and don't have a
        # real clock. All code creation is done on module level.
        for module in self.modules:
            module.generate_code(hdf5_file)
            module.do_checks()
        # After all modules created their output, we collect the digital and analog 
        # data and store them in the hdf5 file.
        self.collect_card_instructions(hdf5_file)

        # Save stop time to ADwin device properties
        self.set_property("stop_time", self.stop_time, "device_properties")
        # Save list of module names to connection table properties
        module_dict = {str(module.module_address) : module.name for module in self.modules}
        self.set_property("modules", module_dict, "connection_table_properties")
        
        # # We don't actually care about these other things that pseudoclock
        # # classes normally do, but they still do some error checking
        # # that we want:
        # outputs, outputs_by_clockline = self._pseudoclock_T12.get_outputs_by_clockline()
        # all_change_times, change_times = self._pseudoclock_T12.collect_change_times(outputs, outputs_by_clockline)

        # for output in outputs:
        #    output.make_timeseries(all_change_times)
        # all_times, clock = self._pseudoclock_T12.expand_change_times(all_change_times, change_times, outputs_by_clockline)
