#####################################################################
#                                                                   #
# ADwinProII/labscript_devices_ADwin_modules.py                     #
#                                                                   #
# Copyright 2022, TU Vienna                                         #
#                                                                   #
# Implementation of the ADwin-Pro II for the labscript-suite,       #
# used in the Léonard lab for Experimental Quantum Information.     #
#                                                                   #
#####################################################################


from labscript import Device, IntermediateDevice, AnalogOut, DigitalOut, AnalogIn, bitfield, LabscriptError, set_passed_properties, fastflatten, set_attributes
import numpy as np

from user_devices.ADwinProII import PROCESSDELAY_TiCo, TiCo_start_cycles, module_start_index
from user_devices.ADwinProII.ADwin_utils import ADC


    
class _ADwinCard(IntermediateDevice):
    """Dummy Class with functionality shared by all ADwin Modules"""
    def __init__(self, name, parent_device, module_address, TiCo=False, PROCESSDELAY_TiCo=None, **kwargs):
        if not type(parent_device).__name__ == "ADwinProII":
            raise LabscriptError(f"The ADwin module {name} needs to be created with ADwin-pro II device as parent, not {parent_device}!")
        self.module_address = module_address
        self.TiCo = TiCo
        self.PROCESSDELAY = PROCESSDELAY_TiCo or parent_device.PROCESSDELAY
        self.adwin_name = parent_device.name
        if TiCo:
            self._TiCo = parent_device.add_TiCo(name,module_address,PROCESSDELAY_TiCo)
            clockline = parent_device.TiCos[name].clockline
        else:
            clockline = parent_device._clockline_T12
        super().__init__(name, clockline, **kwargs)
    
    def do_checks(self):
        pass # TODO: see which checks in old implemetation are necessary 

    def add_device(self, device, num_channels):
        """ Checks if the channel makes sense before calling the standard add_device."""
        try:
            if not 1 <= int(device.connection) <= num_channels:
                raise LabscriptError(f"Connection of {device.name} must be between in [1,{num_channels}].")
        except ValueError:
            raise LabscriptError(f"Connection of {device.name} must be a number (of type string!), not {device.connection}.")
        IntermediateDevice.add_device(self,device)

class ADwinAnalogOut(AnalogOut):
    description = "Analog Output of ADwin-Pro II AOut-8/16"
    def __init__(self, name, parent_device, connection, PID_channel=None, P=0, I=0, D=0, limits=(-10,10), unit_conversion_class=None, unit_conversion_parameters=None, default_value=0, **kwargs):
        AnalogOut.__init__(self, name, parent_device, connection, limits, unit_conversion_class, unit_conversion_parameters, default_value, **kwargs)
        self.PID = {}
        if not PID_channel is None:
            self.PID[0] = {
                "PID_channel": PID_channel,
                "P": P,
                "I": I,
                "D": D,
                "limits": limits,
            }
    def set_PID(self,t,PID_AnalogIn,P,I,D,limits=(-10,10)):
        if PID_AnalogIn is None:
            self.PID[t] = None
        elif (isinstance(PID_AnalogIn,AnalogIn) and isinstance(PID_AnalogIn.parent_device,_ADwinCard)) \
                or isinstance(PID_AnalogIn,int):
            self.PID[t] = {
                "PID_channel": PID_AnalogIn,
                "P": P,
                "I": I,
                "D": D,
                "limits": limits,
            }
            # TODO: If we want to have scale factors for these PIDs, we have to set them here!
        else:
            raise LabscriptError(f"Setting a PID is only possible with a AnalogIn oblect that is child of an ADwin module.")

    def expand_output(self):
        """Shortcut to generate row output from ramps without collecting all change times from pseudoclock."""
        # These two commands create self.times and self.instructions. We also make
        # sure the output at the end of the shot is stord in the output table.
        self.get_change_times()
        times = self.times if self.pseudoclock_device.stop_time in self.times else self.times+[self.pseudoclock_device.stop_time]
        self.make_timeseries(times)
        # We first have to collect all times for the ramps, then we can expand the output values.
        all_times = []
        flat_all_times_len = 0
        for i,t in enumerate(times):
            if isinstance(self.timeseries[i],dict): # Ramps are stored in dictionarys
                ramp_times =np.linspace(
                    self.timeseries[i]['initial time'],
                    self.timeseries[i]['end time'],
                    int(self.timeseries[i]['clock rate'] * (self.timeseries[i]['end time'] - self.timeseries[i]['initial time'])),
                    endpoint=False
                )
                if ramp_times.size == 0:
                    raise LabscriptError(f"The ramp of {self.name} has no change times, increase the ramp duration or samplerate.")
                all_times.append(ramp_times)
                flat_all_times_len += ramp_times.size
            else: 
                all_times.append(t)
                flat_all_times_len += 1
        self.expand_timeseries(all_times,flat_all_times_len)
        # For the output table, we need all times flattened.
        self.all_times = fastflatten(all_times,float)


# There are 32 digital outputs on a card (DIO-32-TiCo)
class ADwinDIO32(_ADwinCard):
    description = 'ADWin-Pro II DIO32-TiCo'
    digital_dtype = np.uint32
    n_digitals = 32
    allowed_children = [DigitalOut] # DigitalIn ?

    @set_passed_properties(
        property_names={
            "connection_table_properties": ["module_address", "PROCESSDELAY_TiCo","num_DO", "num_DI", "DO_ports", "DI_ports"],
            "device_properties": [],
        }
    )
    def __init__(
            self, name, parent_device, module_address, 
            TiCo=True, PROCESSDELAY_TiCo=PROCESSDELAY_TiCo, 
            DO_ports=[], DI_ports=[], **kwargs):
        _ADwinCard.__init__(self, name, parent_device, module_address, TiCo, PROCESSDELAY_TiCo, **kwargs)
        
        self.DO_ports = DO_ports
        self.DI_ports = DI_ports
        if not DI_ports:
            self.DO_ports = [str(i) for i in range(1,33)]
        self.num_DO = len(self.DO_ports)
        self.num_DI = len(self.DI_ports)
    
    def add_device(self, device):
        _ADwinCard.add_device(self, device, self.num_DO) # TODO: DigitalIn ?

    def do_checks(self):
        # Raise an error if any DigitalOut is changed right after start, because the TiCo is only started after some cycles of the ADwin.
        times = self.digital_data["n_cycles"]
        if np.any((times>0) & (times<=TiCo_start_cycles)):
            raise LabscriptError(f"Digital Outputs of {self.name} cannnot change between 0 and {TiCo_start_cycles*self._TiCo.clock_resolution*1e6:.0f}µs.")
        super().do_checks()


    def generate_code(self, hdf5_file):
        Device.generate_code(self, hdf5_file)
        outputs, outputs_by_clockline = self._TiCo.get_outputs_by_clockline() 
        all_change_times,_ = self._TiCo.collect_change_times(outputs,outputs_by_clockline)
        outputarray = [np.zeros(len(all_change_times),dtype=self.digital_dtype)]*self.n_digitals
        
        for output in outputs:
            output.make_timeseries(all_change_times)
            channel = int(output.connection) - 1
            outputarray[channel] = np.array(output.timeseries,dtype=self.digital_dtype)
        
        bits = bitfield(outputarray, dtype=self.digital_dtype)
        
        digital_dtypes = [("n_cycles",np.int32), ('bitfield',np.uint32)]
        self.digital_data = np.empty(len(all_change_times), dtype=digital_dtypes)
        self.digital_data["n_cycles"] = np.array(all_change_times) / self._TiCo.clock_resolution
        self.digital_data['bitfield'] = bits



# Voltages are specified with a 16 bit unsigned integer, mapping the range [-10,10) volts.
# There are 8 analog outputs on a card (AOut-8/16)    
class ADwinAO8(_ADwinCard):
    description = "ADwin-Pro II-AOut-8/16 module"
    allowed_children = [ADwinAnalogOut]
    device_dtype = np.float64
    resolution_bits=16
    min_V = -10
    max_V = 10
    step_size = (max_V-min_V)/2**resolution_bits

    @set_passed_properties(
        property_names={
            "connection_table_properties": ["module_address","adwin_name","num_AO", "resolution_bits", "min_V", "max_V", "step_size", "start_index"],
            "device_properties": [],
        }
    )
    def __init__(self, name, parent_device, module_address, num_AO=8, **kwargs):
        self.num_AO = num_AO
        self.start_index = module_start_index[int(module_address)]
        super().__init__(name, parent_device, module_address, **kwargs)

    def add_device(self, device):
        _ADwinCard.add_device(self, device, self.num_AO)

    def do_checks(self):
        _ADwinCard.do_checks(self)
        # Check if the sample_rate in ramp instructions is below the ADwin clockrate
        # This is necessary here, because we don't call the pseudoclock's expand_change_time()
        for output in self.child_devices:
            for instr in output.instructions.values():
                if isinstance(instr,dict) and instr["clock rate"]>self.parent_device.clock_limit:
                    raise LabscriptError(f"The ramp sample rate ({instr['clock rate']/1e3:.0f}kHz) of {output.name} must not be faster than ADwin ({self.parent_device.clock_limit/1e3:.0f}kHz).")

    def generate_code(self,hdf5_file):
        Device.generate_code(self, hdf5_file)
        clockline = self.parent_device
        pseudoclock = clockline.parent_device

        output_dtypes = [("n_cycles",np.int32),("channel",np.int32),("value",np.int32)]
        PID_dtypes = [("n_cycles",np.int32),("AOUT_channel",np.int32),("PID_channel",np.int32),("PID_P",np.float64),("PID_I",np.float64),("PID_D",np.float64),("PID_min",np.int32),("PID_max",np.int32)]
        outputs = []
        PID_channels = []

        for output in sorted(self.child_devices, key=lambda dev:int(dev.connection)):
            output.expand_output()

            # Get input channels for PID, collect changed for time table and store bare channels as dataset
            if output.PID:
                PID_array = np.zeros(len(output.PID),dtype=PID_dtypes)
                PID_times = np.array(list(output.PID.keys()))
                PID_times.sort()
                PID_array["n_cycles"] = np.round(PID_times * pseudoclock.clock_limit)
                # PID_array["PID_channel"] = list(output.PID_channel.values())
                PID_array["AOUT_channel"] = int(output.connection) + self.start_index
                PID_channels.append(PID_array)
                # If a PID is enabled, the set values are not the actual voltage values of the 
                # Output, but those measured at the input. If the input has a gain enabled, the
                # set values have the be scaled too, to have the PID stabilized to the right values.
                indices = np.digitize(output.all_times, PID_times)
                for i,t in enumerate(PID_times):
                    if isinstance(output.PID[t]['PID_channel'],AnalogIn):
                        PID_array["PID_channel"][i] = int(output.PID[t]['PID_channel'].connection) + output.PID[t]['PID_channel'].parent_device.start_index
                        # Sacle output by gain of PID AIn channel
                        output.raw_output[indices==i+1] *= output.PID[t]['PID_channel'].scale_factor
                    elif isinstance(output.PID_channel[t],int):
                        PID_array["PID_channel"][i] = output.PID[t]['PID_channel']
                    # If PID_channel[t]=None, there is a 0 in the PID_array

                    PID_array["PID_P"][i] = output.PID[t]['P']
                    PID_array["PID_I"][i] = output.PID[t]['I']
                    PID_array["PID_D"][i] = output.PID[t]['D']
                    limits = output.PID[t]['limits']
                    PID_array["PID_min"][i] = ADC(limits[0],self.resolution_bits,self.min_V,self.max_V)
                    PID_array["PID_max"][i] = ADC(limits[1],self.resolution_bits,self.min_V,self.max_V)
            # The ADwin has 16 bit output resolution, so we quantize the Voltage to the right values
            quantized_output = ADC(output.raw_output,self.resolution_bits,self.min_V,self.max_V)
            out = np.empty(quantized_output.size,dtype=output_dtypes)
            out["n_cycles"] = np.round(output.all_times * pseudoclock.clock_limit)
            out["value"] = quantized_output
            out["channel"] = int(output.connection) + self.start_index
            outputs.append(out)

        self.outputs = np.concatenate(outputs) if outputs else np.array([],dtype=output_dtypes)
        self.PID_channels = np.concatenate(PID_channels) if PID_channels else np.array([],dtype=PID_dtypes)



# Voltages are specified with a 16 bit unsigned integer, mapping the range [-10,10) volts.
# There are 8 analog inputs on a card (AIn-F-8/16)
class ADwinAI8(_ADwinCard):
    description = "ADwin-Pro II-AIn-F-8/16 module"
    allowed_children = [AnalogIn]
    resolution_bits=16
    min_V = -10
    max_V = 10
    gain_modes = {1:0,2:1,4:2,8:3} # Mode 0 = Scale factor 1, Mode 1 = Scale factor 2, etc. 

    @set_passed_properties(
        property_names={
            "connection_table_properties": ["module_address","adwin_name","num_AI", "resolution_bits", "min_V", "max_V", "start_index"],
            "device_properties": [],
        }
    )
    def __init__(self, name, parent_device, module_address, num_AI = 8, **kwargs):
        self.num_AI = num_AI
        self.start_index = module_start_index[int(module_address)]
        _ADwinCard.__init__(self, name, parent_device, module_address, TiCo=False, **kwargs)

    def add_device(self, device):
        _ADwinCard.add_device(self, device, self.num_AI)

    def do_checks(self):
        _ADwinCard.do_checks(self)
        # TODO implement further checks if required
        if np.any(self.AIN_times["start_time"] > self.AIN_times["stop_time"]):
            raise LabscriptError(f"Start time for acquisition with Adwin {self.name} must not be later than stop time.")


    def generate_code(self,hdf5_file):
        Device.generate_code(self, hdf5_file)
        AI_group = hdf5_file.require_group(f"/devices/{self.adwin_name}/ANALOG_IN")

        clockline = self.parent_device
        pseudoclock = clockline.parent_device

        self.AIN_times = np.zeros(self.num_AI,dtype = [("start_time",np.int32),("stop_time",np.int32), ("gain_mode",np.int32)])

        # loop through all connected analog in channels and get start and end times
        for analogIn in self.child_devices:
            channel = int(analogIn.connection)
            try:
                self.AIN_times["gain_mode"][channel-1] = self.gain_modes[int(analogIn.scale_factor)]
                analogIn.set_property("scale_factor", int(analogIn.scale_factor), "connection_table_properties")
            except KeyError:
                raise LabscriptError(f"Scale factor for AnalogIn {analogIn.name} must be in {list(self.gain_modes.keys())}, not {analogIn.scale_factor}.")
            if analogIn.acquisitions:
                if len(analogIn.acquisitions)>1:
                    print(f"Warning: For channel {analogIn.name} more than one aquisition was defined, the data is also measured for all times in between!")
                # only a single acquisition is handled by adwin
                start_time = min([aqu["start_time"] for aqu in analogIn.acquisitions])
                stop_time = max([aqu["end_time"] for aqu in analogIn.acquisitions])
                # If the stop_time is later than the ADwin's, stop
                stop_time = min(stop_time,self.pseudoclock_device.stop_time)
                # convert to time steps
                start_time = np.round(start_time * pseudoclock.clock_limit)
                stop_time = np.round(stop_time * pseudoclock.clock_limit)
                self.AIN_times["start_time"][channel-1]=start_time
                self.AIN_times["stop_time"][channel-1]=stop_time
                AI_group.attrs[analogIn.name] = ", ".join([aqu["label"] for aqu in analogIn.acquisitions])
