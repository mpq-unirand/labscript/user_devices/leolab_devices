#####################################################################
#                                                                   #
# ADwinProII/blacs_tabs.py                                          #
#                                                                   #
# Copyright 2022, TU Vienna                                         #
#                                                                   #
# Implementation of the ADwin-Pro II for the labscript-suite,       #
# used in the Léonard lab for Experimental Quantum Information.     #
#                                                                   #
#####################################################################

import os
from labscript import LabscriptError
from blacs.device_base_class import DeviceTab, define_state, MODE_BUFFERED, MODE_MANUAL, MODE_TRANSITION_TO_BUFFERED, MODE_TRANSITION_TO_MANUAL
from blacs.output_classes import DO
from labscript_utils.qtwidgets.digitaloutput import DigitalOutput, InvertedDigitalOutput
from user_devices.ADwinProII.qtwidgets.analoginput import ADwinAnalogInput
from user_devices.ADwinProII.ADwin_utils import get_channel_from_BLACS_name
from qtutils.qt import QtWidgets, QtGui
from qtutils import UiLoader


class ADwinProIITab(DeviceTab):
    def initialise_GUI(self):
        self.event_queue.logging_enabled = True
        connection_table = self.settings['connection_table']
        props = connection_table.find_by_name(self.device_name).properties

        DO_widgets = []
        AO_widgets = []
        AI_widgets = []
        self._AI = {}
        DIO_ADwin_DataNo = [] # Get the DIO modules and their address to know which data to write in ADwin

        # We cannot use all predefined functions of DeviceTab for creating the widgets, becasue
        # the outputs are no child devices of the ADwin directly, but from its modules.
        for module_name in props["modules"].values():
            module = connection_table.find_by_name(module_name)
            module_props = module.properties
            module_widgets = {}
            if module.device_class == "ADwinDIO32":
                DIO_ADwin_DataNo.append((module_name,10*int(module_props["module_address"])))
                for DO_channel in module_props["DO_ports"]:
                    BLACS_name = f"{module_props['module_address']}/{DO_channel}"
                    device = self.get_child_from_connection_table(module_name,DO_channel)
                    inverted = bool(device.properties.get('inverted', False)) if device else False
                    connection_name = device.name if device else '-'
                    self._DO[BLACS_name] = DO(BLACS_name, connection_name, module_name, self.program_device, self.settings)
                    if not inverted:
                        widget = DigitalOutput('%s\n%s'%(DO_channel,connection_name))
                    else:
                        widget = InvertedDigitalOutput('%s\n%s'%(DO_channel,connection_name))
                    self._DO[BLACS_name].add_widget(widget, inverted=inverted)
                    module_widgets[BLACS_name] = widget
                DO_widgets.append((module_name + " Digital Outputs",module_widgets,get_channel_from_BLACS_name))

            elif module.device_class == "ADwinAO8":
                for AO_channel in range(1,module_props["num_AO"]+1):  
                    AO_props = {
                        'base_unit': 'V',
                        'min': module_props["min_V"],
                        'max': module_props["max_V"],
                        'step': module_props["step_size"],
                        'decimals': 5
                    }
                    BLACS_name = f"{module_props['module_address']}/{AO_channel}"              
                    self._AO[BLACS_name] = self._create_AO_object(module_name,BLACS_name,str(AO_channel),AO_props)
                    module_widgets[BLACS_name] = self._AO[BLACS_name].create_widget(display_name=f"{str(AO_channel)}\n{self._AO[BLACS_name]._connection_name}")
                AO_widgets.append((module_name + " Analog Outputs",module_widgets,get_channel_from_BLACS_name))

            elif module.device_class == "ADwinAI8":
                for AI_channel in range(1,module_props["num_AI"]+1):
                    BLACS_name = f"{module_props['module_address']}/{AI_channel}"
                    device = self.get_child_from_connection_table(module_name,str(AI_channel))
                    connection_name = device.name if device else '-'
                    scale_factor = device.properties["scale_factor"] if device else 1
                    module_widgets[BLACS_name] = ADwinAnalogInput(BLACS_name,str(AI_channel),connection_name,scale_factor=scale_factor)
                    self._AI.setdefault(module_props['module_address'],{},)
                    self._AI[int(module_props['module_address'])][AI_channel] = module_widgets[BLACS_name]
                AI_widgets.append((module_name + " Analog Inputs", module_widgets, get_channel_from_BLACS_name))



        self.create_worker(
            "main_worker",
            "user_devices.ADwinProII.blacs_workers.ADwinProIIWorker",
            {
                "process_buffered" : props["process_buffered"],
                "process_manual" : props["process_manual"],
                "DIO_ADwin_DataNo" : DIO_ADwin_DataNo,
                "modules" : props["modules"],
            }
        )
        self.primary_worker = "main_worker"

        # Set the capabilities of this device
        self.supports_remote_value_check(False)
        self.supports_smart_programming(True)

        # Load UI for PID activation in manual MODE_MANUAL
        self.ui_ManualPID = UiLoader().load(os.path.join(os.path.dirname(os.path.realpath(__file__)),"./qtwidgets/ManualPID.ui"))
        self.ui_ManualPID.PID1_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID2_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID3_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID4_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID5_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID6_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID7_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID8_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID9_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID10_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID11_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID12_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID13_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID14_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID15_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID16_channel.valueChanged.connect(self.on_manual_PID_cahnged)
        self.ui_ManualPID.PID1_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID2_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID3_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID4_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID5_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID6_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID7_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID8_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID9_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID10_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID11_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID12_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID13_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID14_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID15_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID16_Max.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID1_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID2_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID3_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID4_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID5_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID6_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID7_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID8_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID9_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID10_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID11_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID12_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID13_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID14_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID15_Min.valueChanged.connect(self.on_min_max_PID_changed)
        self.ui_ManualPID.PID16_Min.valueChanged.connect(self.on_min_max_PID_changed)

        # Load UI for manual PID control
        self.ui_PID = UiLoader().load(os.path.join(os.path.dirname(os.path.realpath(__file__)),"./qtwidgets/PID.ui"))
        self.ui_PID.pushButton_apply.setIcon(QtGui.QIcon(':/qtutils/fugue/arrow-transition'))
        self.ui_PID.pushButton_clear.setIcon(QtGui.QIcon(':/qtutils/fugue/cross'))
        self.ui_PID.pushButton_apply.clicked.connect(self.send_PID_manual)
        self.ui_PID.pushButton_clear.clicked.connect(self.clear_PID_manual)

        # Load UI for workload
        self.ui_workload = UiLoader().load(os.path.join(os.path.dirname(os.path.realpath(__file__)),"./qtwidgets/Workload.ui"))
        
        # Load UI for workload and ADwin processes
        ui = UiLoader().load(os.path.join(os.path.dirname(os.path.realpath(__file__)),"./qtwidgets/Process.ui"))
        # Set process files from props (from globals) and set button behaviour
        ui.lineEdit_buffered_file.setText(props["process_buffered"])
        ui.lineEdit_manual_file.setText(props["process_manual"])
        self.process_files_location = os.path.dirname(props["process_buffered"])
        ui.toolButton_buffered_file.clicked.connect(lambda: self.on_select_process_file_clicked(ui.lineEdit_buffered_file))
        ui.toolButton_manual_file.clicked.connect(lambda: self.on_select_process_file_clicked(ui.lineEdit_manual_file))
        # Connect signals for buttons
        ui.pushButton_load_buffered.clicked.connect(lambda: self.load_process(ui.lineEdit_buffered_file.text(),"buffered"))
        ui.pushButton_start_buffered.clicked.connect(lambda: self.start_process(ui.lineEdit_buffered_file.text()))
        ui.pushButton_stop_buffered.clicked.connect(lambda: self.stop_process(ui.lineEdit_buffered_file.text()))
        ui.pushButton_load_manual.clicked.connect(lambda: self.load_process(ui.lineEdit_manual_file.text(),"manual"))
        ui.pushButton_start_manual.clicked.connect(lambda: self.start_process(ui.lineEdit_manual_file.text()))
        ui.pushButton_stop_manual.clicked.connect(lambda: self.stop_process(ui.lineEdit_manual_file.text()))
        # Add icons
        ui.toolButton_buffered_file.setIcon(QtGui.QIcon(':/qtutils/fugue/folder-open-document'))
        ui.toolButton_manual_file.setIcon(QtGui.QIcon(':/qtutils/fugue/folder-open-document'))
        ui.pushButton_load_buffered.setIcon(QtGui.QIcon(':/qtutils/fugue/arrow-transition'))
        ui.pushButton_start_buffered.setIcon(QtGui.QIcon(':/qtutils/fugue/control'))
        ui.pushButton_stop_buffered.setIcon(QtGui.QIcon(':/qtutils/fugue/control-stop-square'))
        ui.pushButton_load_manual.setIcon(QtGui.QIcon(':/qtutils/fugue/arrow-transition'))
        ui.pushButton_start_manual.setIcon(QtGui.QIcon(':/qtutils/fugue/control'))
        ui.pushButton_stop_manual.setIcon(QtGui.QIcon(':/qtutils/fugue/control-stop-square'))
        self.ui_process = ui

        # Place UI widgets
        hardware_widgets = {"PID":self.ui_PID, "Workload":self.ui_workload, "Process":self.ui_process}
        self.auto_place_widgets(*DO_widgets,*AO_widgets, *AI_widgets, ("PID controls for manual mode", {"Selector": self.ui_ManualPID}), ("Hardware Control", hardware_widgets))


        # Create dict for AIN_values (fill them with None)
        self.AIN_values = {}
        for module in self._AI:
            for port in self._AI[module]:
                self.AIN_values.setdefault(module, {})
                self.AIN_values[module][port] = None
        
        # Start AIN acquisition in manual mode
        self.statemachine_timeout_add(3000, self.update_AIN_values)
    

    @define_state(MODE_BUFFERED,True)  
    def start_run(self,notify_queue):
        yield(self.queue_work(self._primary_worker,'start_run'))
        self.wait_until_done(notify_queue)


    @define_state(MODE_BUFFERED, True)
    def wait_until_done(self, notify_queue):
        done = yield(self.queue_work(self.primary_worker, 'wait_until_done'))
        # Experiment is over. Tell the queue manager about it:
        if done:
            notify_queue.put('done')
        else:
            raise LabscriptError("ADwin was not finished after waiting expected time for shot execution!")

    @define_state(MODE_MANUAL,True)
    def on_manual_PID_cahnged(self,channel):
        channels = []
        channels.append(self.ui_ManualPID.PID1_channel.value())
        channels.append(self.ui_ManualPID.PID2_channel.value())
        channels.append(self.ui_ManualPID.PID3_channel.value())
        channels.append(self.ui_ManualPID.PID4_channel.value())
        channels.append(self.ui_ManualPID.PID5_channel.value())
        channels.append(self.ui_ManualPID.PID6_channel.value())
        channels.append(self.ui_ManualPID.PID7_channel.value())
        channels.append(self.ui_ManualPID.PID8_channel.value())
        channels.append(self.ui_ManualPID.PID9_channel.value())
        channels.append(self.ui_ManualPID.PID10_channel.value())
        channels.append(self.ui_ManualPID.PID11_channel.value())
        channels.append(self.ui_ManualPID.PID12_channel.value())
        channels.append(self.ui_ManualPID.PID13_channel.value())
        channels.append(self.ui_ManualPID.PID14_channel.value())
        channels.append(self.ui_ManualPID.PID15_channel.value())
        channels.append(self.ui_ManualPID.PID16_channel.value())
        yield(self.queue_work(self.primary_worker,"send_ManualModePID",channels))

    @define_state(MODE_MANUAL,True)
    def on_min_max_PID_changed(self,value):
        min = []
        min.append(self.ui_ManualPID.PID1_Min.value())
        min.append(self.ui_ManualPID.PID2_Min.value())
        min.append(self.ui_ManualPID.PID3_Min.value())
        min.append(self.ui_ManualPID.PID4_Min.value())
        min.append(self.ui_ManualPID.PID5_Min.value())
        min.append(self.ui_ManualPID.PID6_Min.value())
        min.append(self.ui_ManualPID.PID7_Min.value())
        min.append(self.ui_ManualPID.PID8_Min.value())
        min.append(self.ui_ManualPID.PID9_Min.value())
        min.append(self.ui_ManualPID.PID10_Min.value())
        min.append(self.ui_ManualPID.PID11_Min.value())
        min.append(self.ui_ManualPID.PID12_Min.value())
        min.append(self.ui_ManualPID.PID13_Min.value())
        min.append(self.ui_ManualPID.PID14_Min.value())
        min.append(self.ui_ManualPID.PID15_Min.value())
        min.append(self.ui_ManualPID.PID16_Min.value())
        max = []
        max.append(self.ui_ManualPID.PID1_Max.value())
        max.append(self.ui_ManualPID.PID2_Max.value())
        max.append(self.ui_ManualPID.PID3_Max.value())
        max.append(self.ui_ManualPID.PID4_Max.value())
        max.append(self.ui_ManualPID.PID5_Max.value())
        max.append(self.ui_ManualPID.PID6_Max.value())
        max.append(self.ui_ManualPID.PID7_Max.value())
        max.append(self.ui_ManualPID.PID8_Max.value())
        max.append(self.ui_ManualPID.PID9_Max.value())
        max.append(self.ui_ManualPID.PID10_Max.value())
        max.append(self.ui_ManualPID.PID11_Max.value())
        max.append(self.ui_ManualPID.PID12_Max.value())
        max.append(self.ui_ManualPID.PID13_Max.value())
        max.append(self.ui_ManualPID.PID14_Max.value())
        max.append(self.ui_ManualPID.PID15_Max.value())
        max.append(self.ui_ManualPID.PID16_Max.value())
        yield(self.queue_work(self.primary_worker,"send_ManualModePID_Min_Max",min,max))


    @define_state(MODE_MANUAL,True)
    def update_AIN_values(self):
        self.AIN_values = yield(self.queue_work(self._primary_worker, "get_AIN_values", self.AIN_values))
        # Set labels to returned values
        for module in self._AI:
            for port in self._AI[module]:
                self._AI[module][port].set_value(self.AIN_values[module][port])



    @define_state(MODE_BUFFERED,False)
    def transition_to_manual(self,notify_queue,program=False):
        DeviceTab.transition_to_manual(self,notify_queue,program=False)
        workload,free_cachable,free_uncachable = yield(self.queue_work(self._primary_worker, "get_workload"))
        self.ui_workload.workload_label.setText(f"{workload*100:.2f} %")
        self.ui_workload.free_cachable_label.setText(f"{free_cachable} kByte")
        self.ui_workload.free_uncachable_label.setText(f"{free_uncachable*1e-3:.3f} MByte")
        self.statemachine_timeout_add(3000, self.update_AIN_values)

    @define_state(MODE_MANUAL,True)
    def transition_to_buffered(self,h5_file,notify_queue): 
        self.statemachine_timeout_remove(self.update_AIN_values)
        DeviceTab.transition_to_buffered(self,h5_file,notify_queue)


    # Functions for manual GUI control
    @define_state(MODE_MANUAL,False)
    def send_PID_manual(self,widget=None):
        channel = self.ui_PID.channel.value()
        P = self.ui_PID.box_p.value()
        I = self.ui_PID.box_i.value()
        D = self.ui_PID.box_d.value()
        yield(self.queue_work(self._primary_worker,"send_PID_manual",channel,P,I,D))

    @define_state(MODE_MANUAL,False)
    def clear_PID_manual(self,widget=None):
        self.ui_PID.channel.setValue(0)
        self.ui_PID.box_p.setValue(0.)
        self.ui_PID.box_i.setValue(0.)
        self.ui_PID.box_d.setValue(0.)
        yield(self.queue_work(self._primary_worker,"send_PID_manual",0,0.,0.,0.))

    @define_state(MODE_MANUAL,False)
    def load_process(self, process, process_type):
        yield(self.queue_work(self._primary_worker,'load_process',process,process_type))

    @define_state(MODE_MANUAL,False)
    def start_process(self, process):
        yield(self.queue_work(self._primary_worker,'start_process',process))

    @define_state(MODE_MANUAL,False)
    def stop_process(self, process):
        yield(self.queue_work(self._primary_worker,'stop_process',process))

    @define_state(MODE_MANUAL,False)
    def on_select_process_file_clicked(self,lineEdit):
        file = QtWidgets.QFileDialog.getOpenFileName(self.ui_process,
                                                    'Select ADwin process file',
                                                    self.process_files_location,
                                                    "ADwin binary files (*.TC*)")
        if type(file) is tuple:
            file, _ = file

        if not file:
            # User cancelled selection
            return
        # Convert to standard platform specific path, otherwise Qt likes forward slashes:
        file = os.path.abspath(file)
        if not os.path.isfile(file):
            error_dialog("No such file %s." % file)
            return
        # Save the containing folder for use next time we open the dialog box:
        self.process_files_location = os.path.dirname(file)
        # Write the file to the lineEdit:
        lineEdit.setText(file)
