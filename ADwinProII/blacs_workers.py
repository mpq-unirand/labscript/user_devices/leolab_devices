#####################################################################
#                                                                   #
# ADwinProII/blacs_workers.py                                       #
#                                                                   #
# Copyright 2022, TU Vienna                                         #
#                                                                   #
# Implementation of the ADwin-Pro II for the labscript-suite,       #
# used in the Léonard lab for Experimental Quantum Information.     #
#                                                                   #
#####################################################################

import time
import sys
import numpy as np
import labscript_utils.h5_lock
import h5py
from blacs.tab_base_classes import Worker
from labscript import LabscriptError, config
from blacs.device_base_class import MODE_BUFFERED,MODE_MANUAL
from labscript_utils import properties

from user_devices.ADwinProII.ADwin_utils import DAC,ADC
from user_devices.ADwinProII import CLOCK_T12

class ADwinProIIWorker(Worker):
    RAISE_EXCEPTIONS = 1
    h5file = None

    def init(self):
        self.timing = None
        self.smart_cache = {"AOUT":None, "PIDs":None, "AIN":None}
        self.smart_cache.update({DIO:None for DIO in self.DIO_ADwin_DataNo})
        self.process_number_buffered = int(self.process_buffered[-1])
        self.process_number_manual = int(self.process_manual[-1])
        global ADwin
        # The ADwin Python module must be either found in the path, or in the standard Windows location
        try: 
            import ADwin
        except ImportError:
            sys.path.append(r"C:\ADwin\Developer\Python")
            import ADwin
        if ADwin.version < "0.18.0":
            raise ImportError("In ADwin.py version < 0.18.0 setting data with numpy arrays is not supported directly. Upgrade version or use SetData_Long(numpy.ndarray.ctypes, ...) in worker.")
        # self.adw = adwDummy() # Testing without connection
        self.adw = ADwin.ADwin(self.device_no, self.RAISE_EXCEPTIONS)
        # If the ADwin responds (return value 0), we don't need to boot
        self.boot()
        self.adw.Load_Process(self.process_buffered)
        self.adw.Load_Process(self.process_manual)
        print("Loaded ADwin processes (buffered and manual).")
        # Check if the PROCESSDELAY from ADwin and labscript match
        if not self.PROCESSDELAY==self.adw.Get_Processdelay(self.process_number_buffered):
            raise LabscriptError(
                f"PROCESSDELAY from labscript ({self.PROCESSDELAY}) does not match with ADwin ({self.adw.Get_Processdelay(self.process_number_buffered)})!"
                )
            

    def boot(self):
        print(f"Booting {self.device_name}...", end="")
        if sys.platform == "win32":
            BTL = self.adw.ADwindir + "adwin12.btl"
        elif sys.platform == "linux":
            BTL = self.adw.ADwindir + "share/btl/adwin12.btl"
        self.adw.Boot(BTL)
        if self.adw.Test_Version():
            raise LabscriptError("Testing Version failed after booting ADwin")
        print("DONE")


    def wait_until_done(self):
        self.logger.debug("ADwin called check_if_done.")
        if getattr(self, 'start_time', None) is None:
            self.start_time = time.time()
        sleep = self.start_time + self.stop_time - time.time()
        if sleep>0:
            time.sleep(sleep)
        while self.adw.Get_Par(1) == 1: # Check if the 'busy' parameter in ADwin was set to 0
            time.sleep(0.0001)
        return True #self.adw.Get_Par(1) == 0



    def start_run(self):
        self.logger.debug("ADwin starting run.")
        self.start_time = time.time()
        self.adw.Start_Process(self.process_number_buffered)


    def get_final_values(self,h5file):
        f = h5py.File(h5file,'r')
        group = f[f"devices/{self.device_name}"]
        final_values = {}
        # AO_channel_map = {}
        for module_address,module in self.modules.items():
            module_props = properties.get(f,module,"connection_table_properties")
            if module_props.get("num_AO",None) is not None:
                # Set all final AO values to zero
                # This is done for two reasons: first in the current ADwin process all AO are set to 0 at the end,
                # second if some channels are not defined in labscript (and not in the h5file), we still get a front panel value of 0.
                final_values.update({f"{module_address}/{i+1}":0 for i in range(module_props["num_AO"])})
                # For mapping (16) AO ADwin channels to format "module/channel", only needed if final lines in this function are uncommented
                # idx_AO8 = module_props["start_index"]
                # AO_channel_map.update({ i+idx_AO8 : f"{module_address}/{i}" for i in range(1,1+module_props["num_AO"])})

            if module_props.get("num_DO",None) is not None:
                # Confusing way to get list of 32 bools from uint32 number:
                # Get last bitfield value (newaxis because view only works with array), view it as list ar uint8, apply
                # numpy.unpackbits to get list of bits (this only works on uint8, hence view), reorder and change to bool 
                bits = np.unpackbits(group[f"DIGITAL_OUT/{module}"]["bitfield"][-1,np.newaxis].view(np.uint8)[::-1]).astype("bool")[::-1]
                # Make dict and update final_values
                final_values.update({f"{module_address}/{i}":bits[int(i)-1] for i in module_props["DO_ports"]})

        # TODO: only get final AO values, if we don't set then zo zero at end of ADwin process
        # stop_time_quantized = np.round(self.stop_time * CLOCK_T12 / self.PROCESSDELAY)
        # AO_data = group["ANALOG_OUT/VALUES"]
        # final_indices = np.nonzero(AO_data["n_cycles"] == stop_time_quantized)[0]
        # for idx in final_indices:
        #    final_values[AO_channel_map[AO_data["channel"][idx]]] = DAC(AO_data["value"][idx])
        f.close()
        return final_values



    def transition_to_buffered(self, device_name, h5file, initial_values, fresh):
        print("Transition to buffered started. ", end="")
        if self.timing is not None:
            print(f"Time since last shot: {time.perf_counter()-self.timing}s.")
        else:
            print()
        self.timing = time.perf_counter()

        self.logger.debug("ADwin called transition_to_buffered.")
        self.adw.Stop_Process(self.process_number_manual)
        self.h5file = h5file
        with h5py.File(h5file, 'r') as f:
            group = f[f"devices/{device_name}"]
            # Get stop time
            self.stop_time = group.attrs["stop_time"]
            # Send stop time to ADwin
            self.adw.Set_Par(2, int(self.stop_time * CLOCK_T12 / self.PROCESSDELAY))
            # Send data to ADwin
            AOUT = group["ANALOG_OUT/VALUES"]
            if fresh or not np.array_equal(AOUT[:],self.smart_cache["AOUT"]):
                print("AOUT programmed.")
                self.smart_cache["AOUT"] = AOUT[:]
                self.adw.SetData_Long(AOUT["n_cycles"], 1, 1, AOUT.shape[0])
                self.adw.SetData_Long(AOUT["channel"], 2, 1, AOUT.shape[0])
                self.adw.SetData_Long(AOUT["value"], 3, 1, AOUT.shape[0])
            for name,module in self.DIO_ADwin_DataNo:
                DOUT = group[f"DIGITAL_OUT/{name}"]
                if fresh or not np.array_equal(DOUT[:],self.smart_cache[name]):
                    print(f"{name} programmed.")
                    self.smart_cache[name] = DOUT[:]
                    self.adw.SetData_Long(DOUT["n_cycles"], module,   1, DOUT.shape[0])
                    self.adw.SetData_Long(DOUT["bitfield"], module+1, 1, DOUT.shape[0])
            PIDs = group["ANALOG_OUT/PID_CHANNELS"]
            if fresh or not np.array_equal(PIDs[:],self.smart_cache["PIDs"]):
                print("PIDs programmed.")
                self.smart_cache["PIDs"] = PIDs[:]
                self.adw.SetData_Long(PIDs["n_cycles"], 4, 1, PIDs.shape[0])
                self.adw.SetData_Long(PIDs["AOUT_channel"], 5, 1, PIDs.shape[0])
                self.adw.SetData_Long(PIDs["PID_channel"], 6, 1, PIDs.shape[0])
                self.adw.SetData_Float(PIDs["PID_P"], 25, 1, PIDs.shape[0])
                self.adw.SetData_Float(PIDs["PID_I"], 26, 1, PIDs.shape[0])
                self.adw.SetData_Float(PIDs["PID_D"], 27, 1, PIDs.shape[0])
                self.adw.SetData_Long(PIDs["PID_min"], 28, 1, PIDs.shape[0])
                self.adw.SetData_Long(PIDs["PID_max"], 29, 1, PIDs.shape[0])
            AIN = group["ANALOG_IN/TIMES"]
            if fresh or not np.array_equal(AIN[:],self.smart_cache["AIN"]):
                print("AIN programmed.")
                self.smart_cache["AIN"] = AIN[:]
                self.adw.SetData_Long(AIN["start_time"], 7, 1, AIN.shape[0])
                self.adw.SetData_Long(AIN["stop_time"], 8, 1, AIN.shape[0])
                self.adw.SetData_Long(AIN["gain_mode"], 9, 1, AIN.shape[0])
        return self.get_final_values(h5file) # return final values to show the right state after transition to manual
    
    

    def transition_to_manual(self):
        self.logger.debug("ADwin called transition_to_manual.")
        # Get AIN measurements from ADwin
        with h5py.File(self.h5file,'r+') as f:
            AI_count = f[f"devices/{self.device_name}/ANALOG_IN"].attrs["AIN_count"]
            group = f.require_group("data/traces/")
            # Read Analog In data from ADwin
            if AI_count>0:
                AIN_data = np.ctypeslib.as_array(self.adw.GetData_Long(99,1,int(AI_count)))
                group.create_dataset("ADwinAnalogIn_DATA", compression = config.compression, data = AIN_data)
            # Workload for Testing
            # stop_time = self.adw.Get_Par(2)-1
            # workload_data = np.ctypeslib.as_array(self.adw.GetData_Long(31,1,stop_time))
            # array = np.empty(stop_time, dtype=[("t",np.int32),("values",np.int32)])
            # array["t"] = np.arange(stop_time)
            # array["values"] = workload_data
            # group.create_dataset("ADwin_Workload", compression = config.compression, data = array)
            # f['devices/ADwin/ANALOG_IN'].attrs["ADwin_Workload"] = "TEST"
        # Delete h5file from worker, shot is finished
        self.h5file = None
        # Check if the TiCo processes were running correctly
        for name,num in self.DIO_ADwin_DataNo:
            if not self.adw.Get_Par(num)==1:
                raise LabscriptError(f"TiCo process of module {name} was not running before at end main process.")
        # Stop buffered and start manual process in ADwin
        self.adw.Stop_Process(self.process_number_buffered)
        #self.adw.Start_Process(self.process_number_manual)
        return True



    def program_manual(self, values):
        print("ADwin Program Manual")
        if self.adw.Process_Status(self.process_number_manual) != 1: # 1 if process is running
            self.adw.Start_Process(self.process_number_manual)
        # Convert dict of frontpanel values to format for ADwin
        data = {}
        for BLACS_name,output in values.items():
            module,channel = map(int,BLACS_name.split('/'))
            if isinstance(output,bool):
                data.setdefault(module,0)
                data[module] += output << (channel-1)
            else:
                data.setdefault(module,np.zeros(8,dtype=np.float64)) # TODO: can we avoid using '8' here?
                data[module][channel-1] = output
        # Send data to ADwin
        for module,module_data in data.items():
            if isinstance(module_data,np.ndarray): # Analog output
                self.adw.SetData_Long(ADC(module_data), 90+module, 1, module_data.size)
            else:
                self.adw.Set_Par(90+module,module_data)
        # If not already, start process_manual at ADwin
        self.adw.Set_Par(11,1)
        return values

    def get_AIN_values(self,AIN_values):
        """
        Read all analog input values from the ADwin modules if we are in manual mode.
        AIN_values : dict
            {modules_address/port: value, ...}
        """
        if self.adw.Process_Status(self.process_number_manual) != 1: # retruns 1 if process is running
            self.adw.Start_Process(self.process_number_manual)
        if self.adw.Get_Par(11) == 1:
            for module_address,inputs in AIN_values.items():
                values = self.adw.GetData_Long(90+int(module_address), 1, len(inputs))
                values = DAC(np.ctypeslib.as_array(values))
                for port in inputs:
                    inputs[port] = values[port-1]
        return AIN_values



    def shutdown(self):
        self.adw.Stop_Process(self.process_number_buffered)
        self.adw.Stop_Process(self.process_number_manual)
        return True


    def abort_transition_to_buffered(self):
        self.adw.Stop_Process(self.process_number_buffered)
        self.adw.Start_Process(self.process_number_manual)
        return True
    

    def abort_buffered(self):
        return self.abort_transition_to_buffered()


    # Functions for manual interaction from BLACS GUI
    def send_PID_manual(self,channel,P,I,D):
        self.adw.Set_Par(21,channel)
        self.adw.Set_FPar(21,P)
        self.adw.Set_FPar(22,I)
        self.adw.Set_FPar(23,D)

    def send_ManualModePID(self,channels):
        self.adw.SetData_Long(channels, 101, 1, len(channels))

    def send_ManualModePID_Min_Max(self,min,max):
        self.adw.SetData_Long(ADC(np.array(min)),102,1,len(min))
        self.adw.SetData_Long(ADC(np.array(max)),103,1,len(max))

    def get_workload(self):
        workload_shot = self.adw.Get_Par(13) / (self.adw.Get_Par(2)-1) / self.PROCESSDELAY
        return workload_shot, self.adw.Free_Mem(5), self.adw.Free_Mem(6)

    def load_process(self,process,process_type):
        print("Load process",int(process[-1]))
        if process_type == "buffered":
            self.adw.Clear_Process(self.process_number_buffered)
            self.adw.Load_Process(process)
            self.process_number_buffered = int(process[-1])
        elif process_type == "manual":
            self.adw.Clear_Process(self.process_number_manual)
            self.adw.Load_Process(process)
            self.process_number_manual = int(process[-1])
        if self.process_number_manual == self.process_number_buffered:
            raise LabscriptError("Having two processes with the same process number on the ADwin is not possible!")
    def start_process(self,process):
        print("Start process",int(process[-1]))
        self.adw.Start_Process(int(process[-1]))
    def stop_process(self,process):
        print("Stop process",int(process[-1]))
        self.adw.Stop_Process(int(process[-1]))
 


# For testing without hardware connection to ADwin, 
# we can use a dummy adw class with the needed functions
class adwDummy(object):
    """ADwin dummy for testing without hardware commection."""
    ADwindir = ""
    def Test_Version(self):
        return 0
    def Load_Process(self,*args):
        pass
    def Start_Process(self,*args):
        pass
    def Stop_Process(self,*args):
        pass
    def Clear_Process(self,*args):
        pass
    def Process_Status(self,*args):
        pass
    def Boot(self,*args):
        pass
    def Workload(self):
        return 0
    def Free_Mem(self,*args):
        return 0
    def Get_Par(self, no):
        if no==1:
            return 0
        elif no==2:
            return 2
        else:
            return 1
    def Get_Processdelay(self,*args):
        from user_devices.ADwinProII import PROCESSDELAY_T12
        return PROCESSDELAY_T12
    def GetData_Long(self,no,start,size):
        import numpy
        vals = ADC(numpy.zeros(size)+10)
        return numpy.ctypeslib.as_ctypes(vals)
    def Set_Par(self,*args):
        pass
    def Set_FPar(self,*args):
        pass
    def SetData_Long(self,*args):
        pass